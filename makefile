run:
	cd shopping-cart && yarn start

setup:
	cd shopping-cart yarn install

up: run-docker-build run-docker-image

run-docker-build:
	cd shopping-cart && docker build . -t react-docker

run-docker-image:
	cd shopping-cart && docker run -p 8000:80 react-docker

clean:
	docker system prune -a