import express from 'express'
import path from 'path'

const app = express()

app.use(express.static(__dirname + '/shopping-cart/build'))

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname + '/shopping-cart/build/index.html'))
})

const port = process.env.PORT || 3000

app.listen(port, () => console.log(`server running on port ${port}`))
