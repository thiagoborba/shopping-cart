In the root of this project I created a server with NodeJs just to run the static files on heroku.

But in a real application, it would be necessary to eject the react scripts to generate a production bundle with adequate performance.

In a production application, statics should be delivered by some statics-specific repository, such as amazon s3, and a CDN should be set up for file distribution.

# Commands

This project uses makefile, docker and yarn.

## Makefile commands

In the project directory, you can run:

### `make run`

run `yarn start` for up the app in the development mode.

### `make setup`

run the `yarn install` command to install all dependencies on this project.

### `make up`

build the app in a docker container. It`s necessary open a browser and accesing <http://localhost:8000>.