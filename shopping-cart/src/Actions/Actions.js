import { categories, products } from '../mock/mock'
import { ActionType } from './ActionsTypes'

let timer

function getProductsList() {
  return {
    type: ActionType.PRODUCTS_ROOT,
    payload: products
  }
}

function getCategoriesList() {
  return {
    type: ActionType.CATEGORIES_ROOT,
    payload: categories
  }
}

function updateCurrentCategory(category) {
  return {
    type: ActionType.UPDATE_CURRENT_CATEGORY,
    payload: category
  }
}

function startTimer (dispatch) {
  timer = setInterval(() => {
    dispatch(decreaseTimer())
  }, 1000)
}

function stopTimer () {
  clearInterval(timer)
}

function addProductToCard(product, cartState) {
  return dispatch => {
    if (!cartState.length) {
      startTimer(dispatch)
    }
    dispatch({
      type: ActionType.ADD_PRODUCT_TO_CART,
      payload: product
    })
  }
}

function removeProductToCard(product, cartState) {
  return dispatch => {
    if (cartState.length === 1) {
      stopTimer()
      dispatch(resetTimer())
    }
    dispatch({
      type: ActionType.REMOVE_PRODUCT_TO_CARD,
      payload: product
    })
  }
}

function increaseProdQtyCard(payload) {
  return {
    type: ActionType.INCREASE_PROD_QTY,
    payload: payload
  }
}

function decreaseProdQtyCard(payload) {
  return {
    type: ActionType.DECREASE_PROD_QTY,
    payload: payload
  }
}

function resetShoppingCart() {
  return dispatch => {
    stopTimer()
    dispatch(resetTimer())
    dispatch({
      type: ActionType.RESET_SHOPPING_CART,
    })
  }
}

function decreaseTimer () {
  return {
    type: ActionType.DECREASE_TIMER,
  }
}

function resetTimer () {
  return {
    type: ActionType.RESET_TIMER,
  }
}

export {
  getProductsList,
  getCategoriesList,
  updateCurrentCategory,
  addProductToCard,
  removeProductToCard,
  increaseProdQtyCard,
  decreaseProdQtyCard,
  resetShoppingCart,
  decreaseTimer,
  resetTimer
}

