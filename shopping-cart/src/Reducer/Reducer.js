import { ActionType } from '../Actions/ActionsTypes'
import { 
  reduceProductsList,
  reduceCategoriesList,
  reduceCurrentCategory,
  reduceAddProductToCard,
  reduceRemoveProductToCard,
  reduceDecreasePrdQty,
  reduceIncreasePrdQty,
  reduceResetShoppingCart,
  reduceDecreaseTimer,
  reduceResetTimer
} from './ReducerImplements'

const INITIAL_STATE = {
  productsList: [],
  categoriesList: [],
  shoppingCart: [],
  currentCategory: '',
  timer: 900000,
}

function Reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ActionType.PRODUCTS_ROOT:
      return { ...state, ...reduceProductsList(state, action) }
    case ActionType.CATEGORIES_ROOT:
      return { ...state, ...reduceCategoriesList(state, action) }
    case ActionType.UPDATE_CURRENT_CATEGORY:
      return { ...state, ...reduceCurrentCategory(state, action) }
    case ActionType.ADD_PRODUCT_TO_CART:
      return { ...state, ...reduceAddProductToCard(state, action) }
    case ActionType.REMOVE_PRODUCT_TO_CARD:
      return { ...state, ...reduceRemoveProductToCard(state, action) }
    case ActionType.INCREASE_PROD_QTY:
      return { ...state, ...reduceIncreasePrdQty(state, action) }
    case ActionType.DECREASE_PROD_QTY:
      return { ...state, ...reduceDecreasePrdQty(state, action) }
    case ActionType.RESET_SHOPPING_CART:
      return { ...state, ...reduceResetShoppingCart() }
    case ActionType.DECREASE_TIMER:
      return { ...state, ...reduceDecreaseTimer(state) }
    case ActionType.RESET_TIMER:
      return { ...state, ...reduceResetTimer(state) }
    default:
      return state
  }
}

export {
  Reducer
} 
