function reduceProductsList(state, action) {
  return { productsList: action.payload }
}

function reduceCategoriesList(state, action) {
  return { categoriesList: action.payload }
}

function reduceCurrentCategory(state, action) {
  return { currentCategory: action.payload }
}

function reduceAddProductToCard(state, action) {
  const { payload: prd } = action
  prd.quantity = 1

  return { 
    shoppingCart: [
      ...state.shoppingCart,
      prd,
    ] 
  }
}

function reduceRemoveProductToCard(state, action) {
  const filtredList = state.shoppingCart.filter(prd => prd.id !== action.payload.id)
  return { shoppingCart: filtredList }
}

function reduceIncreasePrdQty(state, action) {
  const { payload: prd, } = action

  const newCartState = state.shoppingCart.map(item => {
    if (item.id === prd.id) {
      item.quantity += 1
      return item
    } 
    return item
  })

  return { shoppingCart: newCartState }
}

function reduceDecreasePrdQty(state, action) {
  const { payload: prd, } = action

  const newCartState = state.shoppingCart.map(item => {
    if (item.id === prd.id) {
      item.quantity -= 1
      return item
    } 
    return item
  })

  return { shoppingCart: newCartState }
}

function reduceResetShoppingCart() {
  return { shoppingCart: [] }
}

function reduceDecreaseTimer(state) {
  return { timer: state.timer - 1000 }
}

function reduceResetTimer() {
  return { timer: 900000 }
}

export { 
  reduceProductsList,
  reduceCategoriesList,
  reduceCurrentCategory,
  reduceAddProductToCard,
  reduceRemoveProductToCard,
  reduceIncreasePrdQty,
  reduceDecreasePrdQty,
  reduceResetShoppingCart,
  reduceDecreaseTimer,
  reduceResetTimer
}
