import React, { useEffect, useState } from 'react'
import { 
  getProductsList,
  getCategoriesList,
  addProductToCard,
  removeProductToCard,
} from '../../Actions/Actions'
import { useDispatch, useSelector } from 'react-redux'
import Page from '../../components/Page/Page'
import ProductsTable from '../../components/Tables/ProductsTable'
import Select from '../../components/select/Select'
import { Container } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  container: {
    display: 'flex',
    flexDirection: 'column',
    margin: 32,
    overflow: 'hidden',
  }
})

const ProductsPage = () => {
  const styles = useStyles()
  const dispatch = useDispatch()
  const productsList = useSelector(({ productsList }) => productsList)
  const [currentProductsList, setCurrentProductsList] = useState(productsList)
  const categoriesList = useSelector(({ categoriesList }) => categoriesList)
  const currentCategory = useSelector(({ currentCategory }) => currentCategory)
  const shoppingCart = useSelector(({ shoppingCart }) => shoppingCart)

  useEffect(() => {
    dispatch(getProductsList())
    dispatch(getCategoriesList())
  }, [dispatch])

  useEffect(() => {
    setCurrentProductsList(filterProducts())
    // eslint-disable-next-line
  }, [currentCategory, productsList])

  function filterProducts () {
    if(currentCategory === '') return productsList
    const newList = productsList.filter(prod => prod.idCategory === currentCategory)
    return newList
  }

  return (
    <Page>
      <Container
        className={styles.container}
      >
        <Select
          data={categoriesList}
        />
        <ProductsTable
          data={currentProductsList}
          onClickAddToCart={data => dispatch(addProductToCard(data, shoppingCart))}
          onClickRemoveToCart={data => dispatch(removeProductToCard(data, shoppingCart))}
        />
      </Container>
    </Page>
  )
}

export default ProductsPage
