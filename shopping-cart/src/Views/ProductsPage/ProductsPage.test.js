import React from 'react'
import Enzyme, { render } from 'enzyme'
import ProductsPage from './ProductsPage'
import Adapter from 'enzyme-adapter-react-16'
import store from '../../Store/Store'
import { Provider } from 'react-redux'

const Component = () => {
  return (
    <Provider store={store}>
      <ProductsPage />
    </Provider>
  )
}

Enzyme.configure({ adapter: new Adapter() })

describe('Component: ProductsPage', () => {
  it('should display a table', () => {
    const wrapper = render(<Component />)
    expect(wrapper.find('table').length).toBe(1)
  })

  it('should display a input', () => {
    const wrapper = render(<Component />)
    expect(wrapper.find('input').length).toBe(1)
  })
})

