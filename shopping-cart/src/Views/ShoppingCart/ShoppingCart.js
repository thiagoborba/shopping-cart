import { Box, Button, Container, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { Cancel, CheckCircle, ShoppingBasket } from '@material-ui/icons'
import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { decreaseProdQtyCard, increaseProdQtyCard, removeProductToCard, resetShoppingCart } from '../../Actions/Actions'
import Modal from '../../components/Modal/Modal'
import Page from '../../components/Page/Page'
import ShoppingCartTable from '../../components/Tables/ShoppingCartTable'
import { useDidUpdateEffect } from '../../Helpers/ComponentDidMountHelper'
import Timer from '../../components/Timer/Timer'

const useStyles = makeStyles({
  container: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    margin: 32,
    overflow: 'hidden',
  },
  icon: {
    height: 72,
    width: 72,
  },
  timerBox: {
    marginBottom: 16,
    flexDirection: 'row',
    display: 'flex',
    alignItems: 'center',
    width: '60%',
  },
  box: {
    width: '60%',
  }
})

const ShoppingCart = ({ history }) => {
  const styles = useStyles()
  const dispatch = useDispatch()
  const shoppingCart = useSelector(({ shoppingCart }) => shoppingCart)
  const [open, setOpen] = useState(false)
  const timer = useSelector(({ timer }) => timer)

  useEffect(() => {
    if (timer === 0) dispatch(resetShoppingCart())
  }, [timer, dispatch])

  function checkIfCartHaveItems () {
    if (!shoppingCart.length) {
      handleOpen()
    }
  }

  useDidUpdateEffect(handleRemoveItem, checkIfCartHaveItems)

  function handleOpen () {
    setOpen(true)
  }

  function handleClose () {
    setOpen(false)
  }

  function ModalAction () {
    history.goBack()
    handleClose()
    dispatch(resetShoppingCart())
  }

  function handleRemoveItem (data) {
    dispatch(removeProductToCard(data, shoppingCart))
  }

  function handleModalIcon () {
    if (shoppingCart.length) {
      return <CheckCircle style={{ width: 56, height: 56 }} />
    }
    return <Cancel style={{ width: 56, height: 56 }} />
  }

  function handleModaltext () {
    if (shoppingCart.length) {
      return 'Pedido Finalizado com sucesso'
    }
    return 'Pedido não contem Itens'
  }

  return (
    <Page>
      <Modal
        open={open}
        onClose={handleClose}
        icon={handleModalIcon()}
        text={handleModaltext()}
        action={ModalAction}
        labelButton='Fechar'
      />
      <Container
        className={styles.container}
      >
        <Typography align='center' variant="h3" gutterBottom>
          Finalizar Pedido
        </Typography>
        <Box
          className={styles.timerBox}
        >
          <ShoppingBasket className={styles.icon} />
          <Timer time={timer} />
        </Box>
        <Box
          className={styles.box}
        >
          <Typography align='left' variant="h6" gutterBottom>
            Revise seus itens
          </Typography>
        </Box>
        <ShoppingCartTable
          data={shoppingCart}
          onCliclPlus={data => dispatch(increaseProdQtyCard(data))}
          onClickMinus={data => dispatch(decreaseProdQtyCard(data))}
          removeItem={handleRemoveItem}
        />
        <Button
          variant='contained'
          color='primary'
          onClick={() => handleOpen()}
        >
          Finalizar Carrinho
        </Button>
      </Container>
    </Page>
  )
}

export default ShoppingCart
