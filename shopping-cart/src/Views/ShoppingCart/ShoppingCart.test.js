import React from 'react'
import Enzyme, { render } from 'enzyme'
import ShoppingCart from './ShoppingCart'
import Adapter from 'enzyme-adapter-react-16'
import store from '../../Store/Store'
import { Provider } from 'react-redux'

const Component = () => {
  return (
    <Provider store={store}>
      <ShoppingCart />
    </Provider>
  )
}

Enzyme.configure({ adapter: new Adapter() })

describe('Component: ShoppingCart', () => {
  it('should display a table', () => {
    const wrapper = render(<Component />)
    expect(wrapper.find('table').length).toBe(1)
  })

  it('should display a button', () => {
    const wrapper = render(<Component />)
    expect(wrapper.find('button').length).toBe(1)
  })
})

