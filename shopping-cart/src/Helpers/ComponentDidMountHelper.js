import { useEffect, useRef } from 'react'

function useDidUpdateEffect(variableToMonitor, callback, callbackValue = true) {
  const didMountRef = useRef(false)

  useEffect(() => {
    if (didMountRef.current)
      callback(callbackValue)
    else
      didMountRef.current = true
      // eslint-disable-next-line
  }, [variableToMonitor])
}

export {
  useDidUpdateEffect
}