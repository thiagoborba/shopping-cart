import { Backdrop, Box, Button, Container, Fade, Modal as ModalMaterial, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import React from 'react'

const useStyles = makeStyles(theme => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    maxWidth: 600,
    width: '100%',
    justifyContent: 'center',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
}))

const Modal = ({ open, onClose, icon, text, action, labelButton}) => {
  const classes = useStyles()

  return (
    <ModalMaterial
      className={classes.modal}
      open={open}
      onClose={onClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <Container className={classes.paper}>
          <Box
            style={{ margin: 16 }}
          >
            { icon }
          </Box>
          <Typography style={{ margin: 16 }} align='left' variant="h6" gutterBottom>
            { text }
          </Typography>
          <Box>
            <Button
              variant='contained'
              onClick={() => action()}
              style={{ margin: 16 }}
            >
              { labelButton }
            </Button>
          </Box>
        </Container>
      </Fade>
    </ModalMaterial>
  )
}

export default Modal