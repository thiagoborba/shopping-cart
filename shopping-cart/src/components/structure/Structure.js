import { makeStyles } from '@material-ui/core/styles'
import React from 'react'
import TopAppBar from './AppBar/TopAppBar'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexFlow: 'column',
    height: '100vh',
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    overflow: 'hidden'
  },
  childrenContainer: {
    height: '100%',
    position: 'relative',
  },
}))

const Structure = (props) => {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <TopAppBar />
      <main className={classes.content}>
        <div className={classes.childrenContainer}>
          { props.children }
        </div>
      </main>
    </div>
  )
}

export default Structure
