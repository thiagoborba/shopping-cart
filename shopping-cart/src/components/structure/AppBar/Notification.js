import { makeStyles } from '@material-ui/core/styles'
import React from 'react'

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.secondary.main,
    borderRadius: '50%',
    position: 'absolute',
    top: 3,
    left: 0,
    width: 20,
    height: 20,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  info: {
    fontSize: 12,
    margin: 4,
  }
}))

const Notification = ({ data }) => {
  const classes = useStyles()

  return (
    <div
      className={classes.root}
    >
      <span
        className={classes.info}
      >
        { data }
      </span>
    </div>
  )
}

export default Notification