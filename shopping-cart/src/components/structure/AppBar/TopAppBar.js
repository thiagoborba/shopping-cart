import { AppBar, IconButton, Toolbar } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { ArrowBack, ShoppingCart } from '@material-ui/icons'
import React from 'react'
import { useSelector } from 'react-redux'
import { withRouter } from 'react-router-dom'
import Tooltip from '../../ToolTip/ToolTip'
import Notification from './Notification'

const useStyles = makeStyles(() => ({
  iconButton: {
    position: 'relative',
  }
}))

const TopAppBAr = (props) => {
  const { pathname } = props.location
  const shoppingCart = useSelector(({ shoppingCart }) => shoppingCart)
  const classes = useStyles()

  return (
    <AppBar position="static">
      <Toolbar
        style={{
          display: 'flex',
          justifyContent: pathname === '/' ? 'flex-end' : 'flex-start'
        }}
      >
        { pathname === '/shoppingCart' && (
          <IconButton
            onClick={() => props.history.goBack()}
            color='inherit'
          >
            <ArrowBack />
          </IconButton>
        ) }
        { pathname === '/' && (
          <IconButton
            onClick={() => props.history.push('/shoppingCart')}
            color='inherit'
            className={classes.iconButton}
          >
            <Notification
              data={shoppingCart.length}
            />
            <Tooltip
              data={shoppingCart}
            >
              <ShoppingCart />
            </Tooltip>
          </IconButton>
        ) }
      </Toolbar>
    </AppBar>
  )
}

export default withRouter(TopAppBAr)
