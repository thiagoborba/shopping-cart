import { Container } from '@material-ui/core'
import React from 'react'

const Page = ({ children }) => {
  return (
    <Container
      style={{
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
      }}
    >
      { children }
    </Container>
  )
}

export default Page
