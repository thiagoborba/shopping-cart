import { Box, IconButton, Paper, Table, TableBody, TableCell, TableFooter, TableHead, TablePagination, TableRow } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { AddCircle, RemoveCircle } from '@material-ui/icons'
import { styles } from 'ansi-colors'
import React, { useEffect, useState } from 'react'

const useStyles = makeStyles({
  root: {
    overflowX: 'auto',
    margin: 16,
  },
  table: {
    minWidth: 650,
  },
  buttonsContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  }
})

const ShoppingCartTable = ({ data = [], onCliclPlus, onClickMinus, removeItem }) => {
  const classes = useStyles()
  const [total, setTotal] = useState(0)
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(3)

  useEffect(() => {
    getTotal()
    // eslint-disable-next-line
  }, [data])

  function handleChangePage (event, newPage) {
    setPage(newPage)
  }

  function handleChangeRowsPerPage (event) {
    setRowsPerPage(+event.target.value)
    setPage(0)
  }

  function getTotal () {
    const total = data.reduce((amount, currentValue) => {
      const qty = currentValue.quantity
      const price = currentValue.price
      return amount + (qty * price)
    }, 0)
    setTotal(total)
  }

  return (
    <Paper className={classes.root}>
      <Table stickyHeader className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Quantidade</TableCell>
            <TableCell>Nome</TableCell>
            <TableCell>Descrição</TableCell>
            <TableCell align="center">Preço</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row => (
            <TableRow key={row.id}>
              <TableCell>
                <Box
                  className={styles.buttonsContainer}
                >
                  <IconButton onClick={() => {
                    if (row.quantity === 1) {
                      removeItem(row)
                      return
                    }
                    onClickMinus(row)
                  }}>
                    <RemoveCircle/>
                  </IconButton>
                  { row.quantity }
                  <IconButton onClick={() => onCliclPlus(row)}>
                    <AddCircle/>
                  </IconButton>
                </Box>
              </TableCell>
              <TableCell>
                {row.name}  
              </TableCell>
              <TableCell>
                {row.Description}  
              </TableCell>
              <TableCell
                align="center"
              >
                R$ {row.price}
              </TableCell>
            </TableRow>
          )) }
        </TableBody>
        <TableFooter>
          <TableRow>
            <TableCell colSpan={3}>Total</TableCell>
            <TableCell align='center'>{ `R$ ${ total }` }</TableCell>
          </TableRow>
        </TableFooter>
      </Table>
      <TablePagination
        rowsPerPageOptions={[3, 5, 10, 15]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  )
}

export default ShoppingCartTable