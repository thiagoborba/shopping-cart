import { Button, Paper, Table, TableBody, TableCell, TableHead, TablePagination, TableRow } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'

const useStyles = makeStyles({
  root: {
    overflowX: 'auto',
    margin: 16,
  },
  table: {
    minWidth: 650,
  },
})

const ProductsTable = ({ data = [], onClickAddToCart, onClickRemoveToCart}) => {
  const classes = useStyles()
  const shoppingCart = useSelector(({ shoppingCart }) => shoppingCart)
  const currentCategory = useSelector(({ currentCategory }) => currentCategory)
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(3)

  useEffect(() => {
    setPage(0)
  }, [currentCategory])

  function handleChangePage (event, newPage) {
    setPage(newPage)
  }

  function handleChangeRowsPerPage (event) {
    setRowsPerPage(+event.target.value)
    setPage(0)
  }

  return (
    <Paper className={classes.root}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Nome</TableCell>
            <TableCell>Descrição</TableCell>
            <TableCell align="center">Preço</TableCell>
            <TableCell align="center">Ações</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row => (
            <TableRow key={row.id}>
              <TableCell>
                {row.name}  
              </TableCell>
              <TableCell>
                {row.Description}
              </TableCell>
              <TableCell
                align="center"
              >
                R$ {row.price}
              </TableCell>
              <TableCell
                align="center"
              >
                { shoppingCart.includes(row) ? (
                  <Button
                    variant='contained'
                    color='secondary'
                    onClick={() => onClickRemoveToCart(row)}
                  >
                    Remover do Carrinho
                  </Button>
                ) : (
                  <Button
                    variant='contained'
                    color='primary'
                    onClick={() => onClickAddToCart(row)}
                  >
                    Adicionar ao Carrinho
                  </Button>
                ) }
              </TableCell>
            </TableRow>
          )) }
        </TableBody>
      </Table>
      <TablePagination
        rowsPerPageOptions={[3, 5, 10, 15, 50, 100]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  )
}

export default ProductsTable 