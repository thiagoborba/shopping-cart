import { Tooltip, Typography } from '@material-ui/core'
import React from 'react'

const SimpleTooltips = ({ children, placement = 'bottom-start', data = [] }) => {
  return (
    <Tooltip
      placement={placement}
      title={
        <>
          {
            !data.length ? <Typography color="inherit"> Não há itens no carrinho</Typography> : data.map(item => <Typography key={item.id} color="inherit">{ item.Description }</Typography>)
          }
        </>
      }
    >
      { children }
    </Tooltip>
  )
}

export default SimpleTooltips