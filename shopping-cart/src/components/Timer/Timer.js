import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Typography } from '@material-ui/core'
import moment from 'moment'

const useStyles = makeStyles(() => ({
  root:{
    margin: 16
  },
}))

const Timer = ({ time }) => {
  const classes = useStyles()

  return (

    <Typography
      className={classes.root}
      align='left' variant="h6" gutterBottom>
      { `00:${moment.duration(time).minutes()}:${moment.duration(time).seconds()} min restantes` }
    </Typography>
  )
}

export default Timer