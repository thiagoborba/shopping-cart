import { FormControl, InputLabel, MenuItem, Select as SelecMaterial } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { updateCurrentCategory } from '../../Actions/Actions'

const useStyles = makeStyles(() => ({
  formControl: {
    margin: 16,
  },
}))

const Select = ({ data = [] }) => {
  const [labelWidth, setLabelWidth] = useState(0)
  const classes = useStyles()
  const inputLabel = useRef(null)
  const dispatch = useDispatch()
  const currentCategory = useSelector(({ currentCategory }) => currentCategory)

  useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth)
  }, [])

  function handleChange (event) {
    const { value } = event.target
    dispatch(updateCurrentCategory(value))
  }

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <InputLabel ref={inputLabel} id="demo-simple-select-outlined-label">
        Categorias
      </InputLabel>
      <SelecMaterial
        value={currentCategory}
        onChange={handleChange}
        labelWidth={labelWidth}
      >
        <MenuItem value="">
          <em>None</em>
        </MenuItem>
        { data.map(item => (
          <MenuItem key={item.id} value={item.id}>
            { item.name }
          </MenuItem>
        )) }
      </SelecMaterial>
    </FormControl>
  )
}

export default Select