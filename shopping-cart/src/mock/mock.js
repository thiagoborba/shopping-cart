export const products = [
  {
    id: 1,
    idCategory: 1,
    name: 'Coca-Cola',
    Description: 'Coca-Cola lata 350ml',
    price: 3.50,
  },
  {
    id: 2,
    idCategory: 1,
    name: 'Fanta',
    Description: 'Fanta Uva lata 350ml',
    price: 3.50,
  },
  {
    id: 3,
    idCategory: 1,
    name: 'Guarana',
    Description: 'Guarana Antartica lata 350ml',
    price: 4.00,
  },
  {
    id: 4,
    idCategory: 1,
    name: 'Guarana Jesus',
    Description: 'Guarana Jesus 350ml',
    price: 4.50,
  },
  {
    id: 5,
    idCategory: 1,
    name: 'Sprit',
    Description: 'Sprite lata 350ml',
    price: 3.00,
  },
  {
    id: 6,
    idCategory: 1,
    name: 'Charrua',
    Description: 'Charrua lata 350ml',
    price: 4.00,
  },
  {
    id: 7,
    idCategory: 1,
    name: 'Pepsi',
    Description: 'Pepsi lata 350ml',
    price: 2.50,
  },
  {
    id: 8,
    idCategory: 1,
    name: 'Kuat',
    Description: 'Guarana Kuat lata 350ml',
    price: 4.00,
  },
  {
    id: 9,
    idCategory: 1,
    name: 'Fruk',
    Description: 'Fruk Cola lata 350ml',
    price: 2.00,
  },
  {
    id: 10,
    idCategory: 2,
    name: 'Fandangos',
    Description: 'Fandangos sabor churrasco 164g',
    price: 4.00,
  },
  {
    id: 11,
    idCategory: 2,
    name: 'Ruffles',
    Description: 'Ruffles original 57g',
    price: 7.00,
  },
  {
    id: 12,
    idCategory: 2,
    name: 'Cheetos',
    Description: 'Cheetos Parmesão 51g',
    price: 4.00,
  },
  {
    id: 13,
    idCategory: 3,
    name: 'Fini',
    Description: 'Fini Dentaduras 40g',
    price: 4.00,
  },
  {
    id: 14,
    idCategory: 3,
    name: 'Snickers',
    Description: 'Snickers original 52g',
    price: 1.00,
  },
  {
    id: 15,
    idCategory: 3,
    name: 'Kit-Kat',
    Description: 'Kit-Kat original 41g',
    price: 2.00,
  },
]

export const categories = [
  {
    id: 1,
    name: 'Bebidas'
  },
  {
    id: 2,
    name: 'Salgados'
  },
  {
    id: 3,
    name: 'Doces'
  },
]