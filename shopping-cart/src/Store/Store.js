import { createStore, applyMiddleware } from 'redux'
import { Reducer } from '../Reducer/Reducer'
import reduxThunk from 'redux-thunk'

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore)
const store = createStoreWithMiddleware(Reducer)

export default store