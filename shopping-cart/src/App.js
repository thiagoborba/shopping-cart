import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import store from './Store/Store'
import CustomThemeProvider from './theme/theme'
import ProductsPage from './Views/ProductsPage/ProductsPage'
import ShoppingCart from './Views/ShoppingCart/ShoppingCart'
import Structure from './components/structure/Structure'

const App = () => {
  return (
    <CustomThemeProvider>
      <Provider store={store}>
        <Router>
          <Structure>
            <Switch>
              <Route path='/' component={ProductsPage} exact/>
              <Route path='/shoppingCart' component={ShoppingCart}/>
            </Switch>
          </Structure>
        </Router>
      </Provider>
    </CustomThemeProvider>
  )
}

export default App
